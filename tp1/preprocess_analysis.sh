#!/usr/bin/env bash
echo 'Unique special characters'
sed -n 's/.*\(&[a-ZA-Z]*;\).*/\1/p' sentiment_analysis/*.tsv|sort|uniq

echo 'Grossly seeing what smileys are in the text'
grep -i -e ' [:=][p\]os)(\/]' sentiment_analysis/train_data.tsv

