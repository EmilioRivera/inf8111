# Usage
Please put the training (and test) csv into `bike-sharing-system` to allow
the notebooks to gather data.
Necessary packages (and some additional ones have been listed in the
requirements.txt file.
# Experiments
Most (all) of data training is done by the `Training.ipynb` notebook.
We also include the `Visualization.ipynb` for your curiosity.
