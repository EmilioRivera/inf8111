import numpy as np
import pandas as pd
from pathlib import Path
from sklearn.preprocessing import OneHotEncoder
from pandas.api.types import CategoricalDtype

def convert_to_float(x):
    if x == '':
        return np.nan
    val = float(str(x).replace(',','.'))
    return val
column_converters = {
    'Temperature (°C)': convert_to_float,
    'Drew point (°C)': convert_to_float,
    'Visibility (km)': convert_to_float,
    'Pressure at the station (kPa)': convert_to_float
}
dtype_mapping = {
    'Station Code': 'category',
    'Weather': 'category',
}

def get_data(train=True, base_path=''):
    return pd.read_csv(Path('{}/{}.csv'.format(base_path, 'training' if train else 'test')), converters=column_converters, parse_dates=['Date/Hour'], dtype=dtype_mapping)


def load_raw_data():
    return get_data(train=True, base_path='./bike-sharing-system/')


class WeatherDataImputer(object):
    def __init__(self, store_cache_df=True):
        self.df = None
        self.store_cache_df = store_cache_df
        self.previous_hour_delta = pd.Timedelta('-1 hour')
        self.is_fitted = False
        # Columns we can copy from previous time
        self.previous_information_columns = ['Temperature (°C)','Drew point (°C)','Relativite humidity (%)', 'wind direction (10s deg)', 'Wind speed (km/h)', 'Visibility (km)', 'Pressure at the station (kPa)']
    # TODO: Check if fit_transform or transform is really necessary
    # Save this to access a multiindex datetimeindex-ed dataframe: # ddd.loc[pd.to_datetime('2015-07-14 00:00:00')].loc['6921']['Temperature (°C)'])
    
    def fit(self, dataframe):
        if self.is_fitted:
            raise Exception('Warning: WeatherImputer was already fitted')
        if self.store_cache_df:
            self.df = dataframe.copy()
            self.df = self.df.set_index(['Date/Hour', 'Station Code'])
            self.is_fitted = True
    
    def fit_transform(self, dataframe):
        self.fit(dataframe)
        return self.transform(dataframe)
    
    def _find_closest_information(self, information_series):
        current_date, station = information_series['Date/Hour'], information_series['Station Code']
        search_date = information_series['Date/Hour'] + self.previous_hour_delta
        previous_info = self.df.loc[pd.to_datetime(search_date)]
        previous_station_info = previous_info.loc[station]
        if not pd.isna(previous_station_info['Temperature (°C)']):
            # We are almost sure to get the right information
            return previous_station_info[self.previous_information_columns]
        else:
            # TODO: Try to get previous information from another station
            pass
    
    def transform(self, dataframe):
        if not self.is_fitted:
            print('[WeatherImputer]: Impossible to transform if not fitted')
            return None
        output_df = dataframe.copy()
        if self.df is not None:
            missing_info_ix = dataframe['Relativite humidity (%)'].isnull()
            output_df.loc[missing_info_ix, self.previous_information_columns] = dataframe.loc[missing_info_ix].apply(self._find_closest_information, axis=1)
            return output_df

class PreProcessingPipeline(object):
    def __init__(self, impute_weather=True, clean_wind=True, date_processing=None, bin_wind=True):
        self.weather_imputer = WeatherDataImputer(store_cache_df=True) if impute_weather else None
        self.clean_wind = clean_wind
        self.date_processing = date_processing
        self.bin_wind = bin_wind
    def _process(self, dataframe):
        output_df = dataframe.copy()
        if self.weather_imputer:
            output_df = self.weather_imputer.transform(output_df)
        if self.clean_wind:
            output_df['wind direction (10s deg)'] = output_df['wind direction (10s deg)'].replace(36., 0.)
        if self.date_processing:
            day_info = self.date_processing.process(output_df)
            output_df = pd.concat([output_df,day_info], axis=1)
        return output_df
    def fit(self, dataframe):
        if self.weather_imputer is not None:
            if self.weather_imputer.is_fitted:
                raise Exception('Warning: weather_imputer seems to be already fitted! Not allowed for preprocessor')
            self.weather_imputer.fit(dataframe)
            print('[PreprocessingPipeline]: weather imputer fitted')
    def transform(self, dataframe):
        return self._process(dataframe)
    def fit_transform(self, dataframe):
        self.fit(dataframe)
        return self._process(dataframe)

def load_preprocessed_dataframe():
    df = load_raw_data()
    date_proc = DateProcessing()
    pp = PreProcessingPipeline(date_processing=date_proc)
    prep_df = pp.fit_transform(df)
    return prep_df
