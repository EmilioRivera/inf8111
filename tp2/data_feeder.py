from data_loader import load_preprocessed_dataframe, DateProcessing

import numpy as np
import pandas as pd
from pathlib import Path

from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import StandardScaler, OneHotEncoder, MinMaxScaler, OrdinalEncoder, LabelEncoder, KBinsDiscretizer
# date_transformer = Pipeline(steps=[
#     ('')
# ])
z_scaling_pipeline = Pipeline(steps=[
    ('z_scale', StandardScaler())
])
min_max_scaling_pipeline = Pipeline(steps=[
    ('min_max_scale', MinMaxScaler())
])
whole_pipeline = ColumnTransformer(remainder='passthrough', transformers=[
#     ('date_information', OneHotEncoder(sparse=False), ['DayOfWeek', 'HourOfDay']),
    # TODO: This damn version of sklearn can't put missing value indidcators....add_indicator=True, 
#     ('imput_missing', SimpleImputer(strategy='most_frequent'), ['hmdx', 'Wind Chill']),
    ('numeric_z_scale', z_scaling_pipeline, ['Temperature (°C)', 'Drew point (°C)']),
    ('min_max_scale', z_scaling_pipeline, ['Pressure at the station (kPa)']),
    ('missing_val_imputer', SimpleImputer(strategy='most_frequent'), ['wind direction (10s deg)']),
    ('put_in_bins', KBinsDiscretizer(strategy='uniform', encode='onehot-dense', n_bins=6), ['wind direction (10s deg)']),
])

def _load_data():
    pp_df_path = Path('preprocessed_df.csv')
    if pp_df_path.exists():
        print(pp_df_path, 'found in cache. Loading this version')
        _date_proc = DateProcessing(True, True)
        dtype_mapping = {
            'DayOfWeek': _date_proc.day_of_week_dtype,
            'HourOfDay': _date_proc.hour_dtype,
            'Weather': 'category',
            'Station Code': 'category'
        }
        df = pd.read_csv(pp_df_path, parse_dates=['Date/Hour'], dtype=dtype_mapping)
    else:
        print(pp_df_path, 'was not found. Constructing dataframe')
        df = load_preprocessed_dataframe()
        df.to_csv(pp_df_path, index=False)
    return df

def main():
    df = _load_data()
    preprocessed_df = pd.get_dummies(df.drop(['Date/Hour','hmdx', 'Wind Chill', 'Volume', 'Withdrawals','Visility indicator'], axis=1))
    preprocessed_df['wind direction (10s deg)'] = preprocessed_df['wind direction (10s deg)'].fillna(0.)

    X = whole_pipeline.fit_transform(preprocessed_df)
    Y = df['Volume'].values
    print(X.shape, Y.shape)

if __name__ == '__main__':
    main()