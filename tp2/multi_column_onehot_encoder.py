import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.preprocessing import OneHotEncoder, MinMaxScaler, OrdinalEncoder, LabelEncoder


class MultiColumnOneHotEncoder(BaseEstimator, TransformerMixin):
    def __init__(self):
        self.mode = None
        self.encoders = None
            
    def _construct_categories(self, X):
        self.categories_ = X.dtypes[X.dtypes == 'category'].apply(lambda x: x.categories.tolist()).to_dict()
        self.label_encoders = {}
        self.encoders = {}
        for col in self.categories_:
            self.label_encoders[col] = LabelEncoder()

            
    def fit(self, X, y=None, **fit_params):
        if len(X.shape) > 1:
            self.mode = 'multi'
            self._construct_categories(X)
#             self.encoders = {cat: OneHotEncoder(categories=[vals]) for cat, vals in self.categories_.items()}
            for col, vals in self.categories_.items():
                to_fit = np.array(self.categories_[col])
                v = self.label_encoders[col].fit_transform(to_fit)
                print(col, to_fit, v)
                print(self.label_encoders[col].classes_)
                print(type(v), v.tolist())
                self.encoders[col] = OneHotEncoder(categories=[sorted(v.tolist())])
                self.encoders[col].fit(v.reshape(-1,1))
        else:
            # Perharps a series
            self.mode = 'single'
            self.cats = X.dtype.categories.tolist()
            self.one_hot = OneHotEncoder(categories=[sorted(self.cats)])
            to_fit = np.array(self.cats).reshape(-1,1)
            self.one_hot.fit(to_fit)
        return self

    def transform(self, X, y=None):
        if self.mode == 'single':
            if len(X.shape) > 1:
                raise Exception('Cannot fit with multi dimensional data in single mode')
            print('transform')
            n_ar = np.array(X.values).reshape(-1,1)
            return self.one_hot.transform(n_ar)
            # TODO: Complete
        values = []
        for col in X.columns:
            print(col)
            _encoder = self.encoders[col]
            n_ar = np.array(X[col].values).reshape(-1,1)
            labeled = self.label_encoders[col].transform(n_ar).reshape(-1,1)
            print(labeled.shape, labeled[0])
            encoded_col = _encoder.transform(labeled.reshape(-1,1))
            print(encoded_col.shape)
            values.append(encoded_col)
        if len(values) == 1:
            return values[0]
        print([val.shape for val in values])
        from scipy.sparse import hstack
        ret = hstack(values)
        print(ret.shape)
        return ret
            
    def fit_transform(self, X, y=None, **fit_params):
        if fit_params:
            print('Warning: fit_params passed is unused in this class:', fit_params)
        return self.fit(X).transform(X, y)
